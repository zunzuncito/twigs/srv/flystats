
import geoip from 'geoip-lite'


export default class Flytistics {
  constructor(sm, cfg, flyweb, table) {
    this.sm = sm;
    this.cfg = cfg;
    this.flyweb = flyweb;
    this.table = table;
  }

  static async start(sm, cfg) {
    try {
      const pg = sm.get_service_instance("pg");
      const table = await pg.table("flycms_table_flytistics");


      const flyweb = sm.get_service_instance("flyweb");

      const _this = new Flytistics(sm, cfg, flyweb, table);

      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async track(req) {
    try {
      const table = this.table;
      const route_path = req.url.split("?")[0];

      if (
        (
          route_path == "/" ||
          route_path.endsWith("index.html")
        ) && this.flyweb.route_mg.get_context(route_path)
      ) {
        const date_str = (new Date()).toISOString().slice(0, 10);
        const client_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        const geo_info = geoip.lookup(client_ip);
        const geo = geo_info ? geo_info.country : "unknown"; // geo.country
        const referer = req.headers.referer || "unknown";
        const referer_domain = referer.replace(/^(https?:)?\/\/|\/$/g, '').split("/")[0];

        const existing = await table.select("*",
          "date = $1 AND route = $2",
          [
            date_str,
            route_path
          ]
        );

        const visit_string = `${geo} ${referer_domain}`;
        console.log("visit_string", visit_string);

        if (existing.length > 0) {

          if (existing[0].visits.includes(visit_string)) {
            const visit_index = existing[0].visits.indexOf(visit_string);
            existing[0].visit_counts[visit_index] = parseInt(existing[0].visit_counts[visit_index]) + 1;
          } else {
            existing[0].visits.push(visit_string);
            existing[0].visit_counts.push(1)
          }

          console.log("FLYTISTICS UPDATE", existing[0]);
          
          await table.update([
            "visits = $1",
            "visit_counts = $2"
          ], "id = $3", [
            existing[0].visits,
            existing[0].visit_counts,
            existing[0].id
          ]);
        } else {
          console.log("FLYTISTICS INSERT", date_str, route_path, [visit_string], [1]);
          await table.insert([
            "date", "route", "visits", "visit_counts"
          ], [
            "$1", "$2", "$3", "$4"
          ], [ 
            date_str, route_path, [visit_string], [1]
          ]);
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
