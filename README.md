
# `srv/flystats` - Anonymous Tracking Service for Marketing Analytics
Designed to comply with GDPR and CCPA regulations and to protect personal user data.
With `flystats` tracker no user consent is required, since all the data saved is purely anonymous.
